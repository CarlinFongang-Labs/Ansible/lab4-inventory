# Ansible | Redaction d'inventaires et usage des variables

_______

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com

><img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://githut.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Contexte
Dans ce laboratoire, nous allons exploiter la notion de variables dans la rédaction des fichiers d'inventaire sous différents formats (INI & YAML).

## Objectifs

Dans ce laboratoire, nous allons :
- Créer un cluster constitué de deux instances (une hôte Ansible et une machine cliente).

- Nous créerons un fichier `hosts.ini` au format INI avec les modalités d'inventaire suivantes :

  - L'instance cliente devra faire partie d'un groupe appelé "prod".

  - Nous utiliserons une seule clé SSH pour la connexion aux instances du groupe "prod".

  - La valeur de la variable "env" devra être égale à "production" pour toutes les instances du groupe "prod".

- Nous créerons ensuite un fichier `hosts.yml`, qui sera la version YAML du précédent fichier INI.

- Enfin, nous testerons les commandes "ad-hoc" (ping et setup) avec chacun des fichiers d'inventaire INI et YAML.

## Prérequis
Avoir deux machines avec Ubuntu déjà installé.

Dans notre cas, nous allons provisionner des instances EC2 s'exécutant sous Ubuntu via AWS, sur lesquelles nous effectuerons toutes nos configurations.

Documentation complémentaire :

[Documentation Ansible](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/yum_module.html)

[Lancer une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws)


## Création des instances EC2 et installation d'Ansible
1. Créer des instances sur AWS :
[Lancer une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws)

2. Installer Ansible :
[Installer Ansible sur une instance Linux](https://gitlab.com/CarlinFongang-Labs/Ansible/lab1-install-ansible)


## 2. Création du fichier hosts.ini et exécution des modules ping, copy et setup

### 2.1. Création du fichier hosts.ini

````bash
ssh -i devops-aCD.pem ubuntu@public_ip_ec2_ansible
nano hosts.ini
````

# Contenu du fichier hosts.ini

````bash
[all:vars]
ansible_user=ubuntu

[prod]
srv_prod ansible_host=44.217.105.61

[prod:vars]
ansible_ssh_private_key_file=.secret/devops-aCD.pem
env=production
````
Ce script INI définit une configuration générale applicable à toutes les machines cibles ([all:vars]), spécifiant que l'utilisateur par défaut pour les connexions SSH est 'ubuntu'. 

Ensuite, il définit un groupe nommé 'prod' contenant une machine cible appelée 'srv_prod' avec son adresse IP spécifique. Pour ce groupe 'prod', il définit également des variables spécifiques : le chemin de la clé privée SSH utilisée pour la connexion (`ansible_ssh_private_key_file`) et une variable d'environnement `env` définie sur 'production'. Ce fichier organise les informations nécessaires pour gérer et exécuter des commandes Ansible sur les groupes d'hôtes, ici focalisé sur un environnement de production.

Voici votre texte corrigé :

### 2.2. Test du module **ping**

```bash
ansible -i hosts.ini prod -m ping # Ping vers les machines du groupe "prod"
```

>![alt text](img/image.png)
*Ping vers les instances du groupe **prod***

```bash
ansible -i hosts.ini srv_prod -m ping # Ping vers l'instance "srv_prod"
```

>![alt text](img/image-1.png)
*Ping vers la machine portant le tag **srv_prod***

### 2.3. Usage du module **setup**

```bash
ansible -i hosts.ini prod -m setup
```

>![alt text](img/image-2.png)
*Informations sur les instances du groupe **prod***

### 2.4. Usage du module **copy**

```bash
ansible -i hosts prod -m copy -a "dest=/home/ubuntu/test4.txt content='hello aCloud.Digital, welcome in a {{ env }} environment'"
```

Pour rappel, **env** est une variable dont la valeur est "production", et cette variable est affectée à toutes les instances du groupe **prod** comme décrit dans le fichier **hosts.ini**.

>![alt text](img/image-3.png)
*Création et copie du fichier **test4.txt** vers les instances du groupe **prod***

### 2.5. Vérification du fichier depuis une instance du groupe **prod**

```bash
ssh -i devops-aCD.pem ubuntu@public_ip_ec2_prod
ls
cat test4.txt
```

>![alt text](img/image-4.png)
*Le fichier **test4.txt** a bien été créé, et la variable a retourné le bon résultat **"production"***

## 3. Création du fichier hosts.yml et exécution des modules ping, setup et copy

### 3.1. Création du fichier hosts.yml

```bash
ssh -i devops-aCD.pem ubuntu@public_ip_ec2_ansible
nano hosts.yml
```

# Contenu du fichier hosts.yml

```bash
all:
  vars:
    ansible_user: ubuntu

prod:
  hosts:
    srv_prod:
      ansible_host: 52.6.219.176
  vars:
    ansible_ssh_private_key_file: ".secret/devops-aCD.pem"
    env: production
```

### 2.2. Test du module **ping**

```bash
ansible -i hosts.yml prod -m ping # Ping vers les machines du groupe "prod"
```

>![alt text](img/image-5.png)
*Ping vers les instances du groupe **prod***

```bash
ansible -i hosts.yml srv_prod -m ping # Ping vers l'instance "srv_prod"
```

>![alt text](img/image-6.png)
*Ping vers la machine portant le tag **srv_prod***

### 2.3. Usage du module **setup**

```bash
ansible -i hosts.yml prod -m setup
```

>![alt text](img/image-7.png)
*Informations sur les instances du groupe **prod***

### 2.4. Usage du module **copy**

```bash
ansible -i hosts.yml prod -m copy -a "dest=/home/ubuntu/test5.txt content='hello aCloud.Digital, welcome in a {{ env }} environment'"
```

Pour rappel, **env** est une variable dont la valeur est "production", et cette variable est affectée à toutes les instances du groupe **prod** comme décrit dans le fichier **hosts.yml**.

>![alt text](img/image-8.png)
*Création et copie du fichier **test5.txt** vers les instances du groupe **prod***

### 2.5. Vérification du fichier depuis une instance du groupe **prod**

```bash
ssh -i devops-aCD.pem ubuntu@public_ip_ec2_prod
ls
cat test5.txt
```

>![alt text](img/image-9.png)
*Le fichier **test5.txt** a bien été créé, et la variable a retourné le bon résultat **"production"***